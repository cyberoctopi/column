(defproject column/lein-template "0.1.1"
  :description "column sits on top of pedestal to support your projects "
  :url "https://gitlab.com/demonshreder/column"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :eval-in-leiningen true)
