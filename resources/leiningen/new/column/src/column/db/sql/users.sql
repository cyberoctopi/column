-- :name users-insert :! :n
-- :doc Insert a single user returning affected row count
insert into users
values :id

-- :name users-by-id :? :1
-- :doc Get user by id
select * from users
where id = :id
