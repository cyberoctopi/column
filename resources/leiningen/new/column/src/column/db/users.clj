(ns {{name}}.db.users
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "{{name}}/db/sql/users.sql")
