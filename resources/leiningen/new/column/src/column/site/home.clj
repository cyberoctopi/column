(ns {{thiral}}.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [{{thiral}}.site :as site]
            ))

(defn home-page [request]
  (ring-resp/response (site/base "home")))
