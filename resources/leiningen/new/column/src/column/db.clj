(ns {{name}}.db
  (:require [mount.core :refer [defstate]]
            [hikari-cp.core :refer :all]
            [clojure.java.jdbc :as jdbc]
            [ragtime.jdbc :as ragtime]
            ))


(def jdbc (:jdbc-url (clojure.edn/read-string (slurp "resources/env/jdbc.edn"))))

(def datasource-options {:jdbc-url "jdbc:postgresql://localhost/database?user=user123&password=pass123"})

(defonce datasource
  (delay (make-datasource datasource-options)))

(defstate conn :start (jdbc/with-db-connection [conn {:datasource @datasource}])
  :stop (close-datasource @datasource))
;(jdbc/with-db-connection [conn {:datasource @datasource}])

 (def config
   {:datastore  (ragtime/sql-database {:connection-uri jdbc })
;;   {:datastore  conn
    :migrations (ragtime/load-resources "migrations")})
