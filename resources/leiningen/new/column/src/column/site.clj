(ns {{thiral}}.site
  (:require  [ring.util.response :as ring-resp])
  (:use
    [hiccup.page :only (html5 include-css include-js)]))
 
(defn base [title & content]
  (html5 {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
          [:link {:rel "icon" :type "image/png" :href "/img/favicon.png"}]
          [:title title]
          (include-css "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css")]
         content))
