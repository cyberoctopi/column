(ns leiningen.new.column
  (:require [leiningen.new.templates :refer [renderer name-to-path ->files]]
            [leiningen.core.main :as main]))

(def render (renderer "column"))

(defn column
  "FIXME: write documentation"
  [name]
  (let [data {:name name
              :sanitized (name-to-path name)}]
    (main/info "Generating fresh 'lein new' column project.")
    (->files data
             ["README.md" (render "README.md" data)]
             ["ARCHITECTURE.md" (render "ARCHITECTURE.md" data)]
             ["CHANGELOG.md" (render "CHANGELOG.md" data)]
             ["CONTRIBUTING.md" (render "CONTRIBUTING.md" data)]
             [".gitignore" (render ".gitignore" data)]
             ["resources/env/jdbc.edn" (render "resources/env/jdbc.edn" data)]
             ["resources/migrations/001-create-users.edn" (render "resources/migrations/001-create-users.edn" data)]
             ["src/{{sanitized}}/server.clj" (render "src/column/server.clj" data)]
             ["src/{{sanitized}}/service.clj" (render "src/column/service.clj" data)]
             ["src/{{sanitized}}/site.clj" (render "src/column/site.clj" data)]
             ["src/{{sanitized}}/site/home.clj" (render "src/column/site/home.clj" data)]
             ["src/{{sanitized}}/db.clj" (render "src/column/db.clj" data)]
             ["src/{{sanitized}}/db/users.clj" (render "src/column/db/users.clj" data)]
             ["src/{{sanitized}}/db/sql/users.sql" (render "src/column/db/sql/users.sql" data)]
             )))
